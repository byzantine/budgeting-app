# How to run the budgeting app locally


1. Clone the repository
2. Setup a Python virtual environment:  
   ```cd budgeting-app```  
   ```virtualenv venv```  
   ```source venv/bin/activate```  
   ```pip install -r requirements.txt```  
4. Open ```app/config.py``` in a text editor and set ```SECRET_KEY```    
   ```SENTRY_DSN```, ```MAILGUN_BASE_URL``` and ```MAILGUN_API_KEY``` are only
   needed if you want to track errors with Sentry and receive mail from the app.
5. Run ```python db_create.py``` to create the database.  By default a local
   SQLite db will be created in the ```app/``` folder.
6. Run ```python run.py``` to start the app, which should now be visible in your
   browser at http://localhost:5000/
