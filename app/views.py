from datetime import datetime as dt
from flask import abort, flash, g, redirect, render_template, request, session
from flask_login import login_user, logout_user, current_user, \
    login_required
from werkzeug import check_password_hash, generate_password_hash
from app import app, db, lm, sentry
from app.constants import *
from .emails import *
from .config import DEBUG
from .forms import *
from .models import *


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    session.modified = True
    g.debug = DEBUG
    g.user = current_user


@app.route('/', methods=['GET', 'POST'])
@login_required
def index():
    form = PaymentForm()

    # Saving a new transaction.
    if form.validate_on_submit():
        payment = Payment(g.user.id,
                          form.payment_name.data,
                          amount=form.payment_amount.data,
                          action=form.payment_action.data,
                          date=dt.strptime(form.payment_date.data, '%d/%m/%Y'),
                          repeat=form.payment_repeat.data)
        db.session.add(payment)
        db.session.commit()
        return redirect(url_for('index'))
    elif form.errors:
        try:
            'CSRF token missing' in form.errors.get('csrf_token')
            flash('The form security token has expired.  Please reload the '
                  'page and try again.', 'error')
        except TypeError:
            flash('The payment could not be saved because of errors in the '
                  'form.', 'error')

    return render_template('forms/dashboard.html',
                           title='Budgeting app',
                           payments=g.user.active_payments(),
                           form=form,
                           repeat=REPEAT,
                           income_this_month=g.user.income_this_month(),
                           expenses_this_month=g.user.expenses_this_month(),
                           income_this_year=g.user.income_this_year(),
                           expenses_this_year=g.user.expenses_this_year(),
                           )


@app.route('/accounts/', methods=['GET', 'POST'])
@login_required
def accounts():
    pass


@app.route('/expense/<int:expense_id>/<action>/')
@login_required
def expense_edit(expense_id, action):
    # Ensure expense belongs to current user.
    try:
        expense = g.user.get_payment(expense_id)
    except KeyError:
        flash('Cannot find specified payment.')
        return redirect(url_for('index'))

    if action not in ['delete', 'activate', 'undo']:
        abort(404)

    if action == 'delete':
        expense.cancel()
        performed = 'Deleted'
    if action in ['activate', 'undo']:
        expense.activate()
        performed = 'Activated'

    db.session.add(expense)
    db.session.commit()
    flash('{} payment.'.format(performed))

    return redirect(url_for('index'))


@app.route('/account/change_password/', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePassword()
    if form.validate_on_submit():
        g.user.password = generate_password_hash(form.newpassword.data)
        db.session.add(g.user)
        db.session.commit()
        flash('Password updated.')

    return render_template('account/change_password.html',
                           active='change_password', form=form,
                           title="My account")


@app.route('/signup/', methods=['GET', 'POST'])
def sign_up():
    if g.user.is_authenticated:
        return redirect(url_for('index'))

    form = Signup()
    title = 'Create an account'
    email_sent = False

    # Inactive users are sent here after trying to login.
    email = request.args.get('email')
    if email:
        # If the user has a new account and no password set.
        user = User.query.filter_by(email=email, status=NEW,
                                    password=None).first()
        if user:
            form.name.data = user.name
            form.email.data = user.email

    if form.validate_on_submit():
        email = form.email.data
        # check if email address already in database
        user = User.query.filter_by(email=email).first()
        if user:
            if user.status in [ACTIVE, INACTIVE]:
                send_dupe_email(user.email)
                flash(u'Someone with the email {} already has an account '
                      'with us.  Did you mean to login?'.format(user.email))
            # TODO: THIS IS BAD, REPLACE WITH ITSDANGEROUS LINKS
            if user.status == NEW:
                # Update user's name and set their password.
                user.name = form.name.data
                user.password = generate_password_hash(form.password.data)
        else:
            # Create new account and log them in.
            user = User(email=email, name=form.name.data,
                        password=generate_password_hash(form.password.data))
            user.status = ACTIVE
            db.session.add(user)
            db.session.commit()
            if login_user(user):
                session.permanent = True
                user.last_auth = dt.utcnow()
                flash(u'Your account has been activated. Welcome {}!'
                      .format(user.name))

        db.session.add(user)
        db.session.commit()
        # send email to welcome new customer
        send_register_email(form.email.data)
        email_sent = True
        title = 'Thanks for signing up!'

    return render_template('forms/signup.html', form=form, title=title,
                           email_sent=email_sent, email=email)


@app.route('/register/resend_register_email/')
def resend_register_email():
    email = request.args.get('email')
    user = User.query.filter_by(email=email, status=1).first()
    if user:
        send_register_email(email)
        flash('We\'ve sent you another welcome email.')
    else:
        flash('Invalid email your account is already active.', 'error')
    return redirect(url_for('index'))


@app.route('/account/verify_email/', defaults={'payload': None})
@app.route('/account/verify_email/<payload>/')
def verify_email(payload):
    # if the signature and payload are valid, set account status to 2:active
    try:
        email = link_is_authorised(payload, max_age=3600).get('email')
    except:
        flash('Invalid or expired URL, please contact support.', 'error')
        return redirect(url_for('index'))

    user = User.query.filter_by(email=email).first()
    user.status = ACTIVE
    db.session.add(user)
    db.session.commit()
    if login_user(user):
        session.permanent = True
        user.last_auth = dt.utcnow()
        db.session.add(user)
        db.session.commit()
        flash(u'Your account has been activated. Welcome {}!'
              .format(user.name))
        return redirect(url_for('index'))
    flash('Unable to login, please contact support.', 'error')
    return redirect('login')


@app.route('/login/', methods=['GET', 'POST'])
def login():
    form = Login()

    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        # Get user to finish account setup if they have partial account.
        if user and user.status == NEW and user.password is None:
            flash('It looks like you already have an account with us.  '
                  'Please complete your account setup.')
            return redirect(url_for('sign_up', email=user.email))

        # use werkzeug to validate user's password
        if user and check_password_hash(user.password, form.password.data):
            # if user has two-factor authentication setup
            if user.tfa_enabled:
                # add user email to session
                session['email'] = user.email
                return redirect(url_for('verify_2fa'))
            if login_user(user):
                session.permanent = True
                user.last_auth = dt.utcnow()
                db.session.add(user)
                db.session.commit()
                # redirect to original destination, if logged-in en route
                return redirect(url_for('index'))
            flash('Unable to login - please contact support.', 'error')
            return redirect('login')
        # bad login
        flash('Incorrect email or password.', 'error')
    return render_template('forms/login.html', form=form,
                           title='Login to your account')


@app.route('/forgot/', methods=['GET', 'POST'])
def forgot():
    form = ForgotPassword()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        # if there's a user with that email address
        if user:
            # send email containing itsdangerous link
            send_reset_link(user.email)
        # display same message whether or not email sent
        flash('We\'ve sent you a password reset link.  Please \
            check your inbox.')
        return redirect(url_for('index'))
    return render_template('forms/forgot.html', form=form,
                           title='Reset your password')


@app.route('/reset_password/<payload>/', methods=['GET', 'POST'])
def reset_password(payload):
    # Check the signature and payload are valid.
    if not link_is_authorised(payload, max_age=3600):
        flash('Invalid or expired URL.', 'error')
        return redirect(url_for('forgot'))

    form = ResetPassword()
    if form.validate_on_submit():
        email = link_is_authorised(payload, max_age=3600).get('email')
        user = User.query.filter_by(email=email).first()
        user.password = generate_password_hash(form.newpassword.data)
        db.session.add(user)
        db.session.commit()
        flash('Password updated.')

        # if user has two-factor authentication setup
        if user.tfa_enabled:
            return redirect(url_for('verify_2fa'))
        if login_user(user):
            session.permanent = True
            user.last_auth = dt.utcnow()
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('index'))
        flash('Unable to login - please contact support.', 'error')
        return redirect('login')

    return render_template('forms/reset_password.html', form=form,
                           payload=payload, title='Reset password')


@app.route('/logout/')
@login_required
def logout():
    logout_user()
    # display message to user
    flash('You have been securely logged out.')
    return redirect(url_for('index'))


###############################################################################

@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html', event_id=g.sentry_event_id,
                           public_dsn=sentry.client.get_public_dsn('https'))
