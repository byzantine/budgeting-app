You received this email because someone tried to register a new account with budgeting app using the email {{ email }}.

You already have an account with us using that email address.  If you're unable to login, you can reset your passphrase with the following link to regain access:
{{ link }}

If you did *not* try to register a new account, just ignore this email.
