import os

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_TRACK_MODIFICATIONS = True
WTF_CSRF_ENABLED = True

# Environment variables only available on heroku.
try:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URI']
    SECRET_KEY = os.environ['SECRET_KEY']
    DEBUG = False
    SENTRY_DSN = os.environ['SENTRY_DSN']
    SENTRY_ENVIRONMENT = 'production'
    MAILGUN_BASE_URL = os.environ['MAILGUN_BASE_URL']
    MAILGUN_API_KEY = os.environ['MAILGUN_API_KEY']
except:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    '''
    Generate a secret key by running the following:
    >>> import os
    >>> os.urandom(24)
    '''
    SECRET_KEY = ''
    DEBUG = True
    SENTRY_DSN = ''
    SENTRY_ENVIRONMENT = 'development'
    MAILGUN_BASE_URL = ''
    MAILGUN_API_KEY = ''

# Email stuff
ADMINS = ['you@example.com']
SUPPORT = 'Support <someone@example.com>'
NOTIFICATIONS = 'Notifications <no-reply@example.com>'
