from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from raven.contrib.flask import Sentry
from werkzeug.contrib.fixers import ProxyFix
from .config import SENTRY_DSN
from .momentjs import moment


app = Flask(__name__)
app.config.from_object('app.config')
app.jinja_env.globals['moment'] = moment
app.wsgi_app = ProxyFix(app.wsgi_app)

# SQLAlchemy stuff
db = SQLAlchemy(app)

# Flask-Login
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
# if the User-Agent or IP changes, session will be destroyed
# lm.session_protection = "strong"      # TODO: BREAKS WTFORMS CSRF PROTECTION
lm.session_protection = "basic"

# Sentry error logging
sentry = Sentry(app, dsn=SENTRY_DSN)

# import statement at end to avoid circular reference; views module imports app
from app import views
