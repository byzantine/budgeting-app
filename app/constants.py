# user role
ADMIN = 0
STAFF = 1
USER = 2
ROLE = {
    ADMIN: 'admin',
    STAFF: 'staff',
    USER: 'user',
}

# activity levels for different entities
INACTIVE = 0
NEW = 1
ACTIVE = 2
STATUS = {
    INACTIVE: 'inactive',
    NEW: 'new',
    ACTIVE: 'active',
}

DAILY = 0
WEEKLY = 1
TWO_WEEKLY = 2
FOUR_WEEKLY = 3
MONTHLY_SAME = 4
MONTHLY_LAST = 5
# TWO_MONTHLY = 6
# THREE_MONTHLY = 7
# SIX_MONTHLY = 8
ANNUAL = 9
REPEAT = {
    DAILY: 'Daily',
    WEEKLY: 'Every week',
    TWO_WEEKLY: 'Every 2 weeks',
    FOUR_WEEKLY: 'Every 4 weeks',
    MONTHLY_SAME: 'Every month (Same Date)',
    MONTHLY_LAST: 'Every month (Last Business Day)',
    # TWO_MONTHLY: 'Every 2 months',
    # THREE_MONTHLY: 'Every 3 months',
    # SIX_MONTHLY: 'Every 6 months',
    ANNUAL: 'Every year',
}

DEBIT = 0
CREDIT = 1
ACTION = {
    DEBIT: 'Debit',
    CREDIT: 'Credit',
}
