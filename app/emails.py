from flask import render_template, url_for
from itsdangerous import URLSafeTimedSerializer
import requests

from app import app
from .config import NOTIFICATIONS, MAILGUN_BASE_URL, MAILGUN_API_KEY
from .decorators import async


def get_serializer(secret_key=None):
    if secret_key is None:
        secret_key = app.secret_key
    # can't think of any reason *not* to use timestamp sigs for everything
    # it's not like they themselves expire
    return URLSafeTimedSerializer(secret_key)


def get_authorised_link(destination, **kwargs):
    s = get_serializer()
    payload = s.dumps(kwargs)
    return url_for(destination, payload=payload, _external=True)


def link_is_authorised(payload, max_age=None):
    s = get_serializer()
    payload_data = {}
    try:
        payload_data = s.loads(payload, max_age=max_age)
    except:
        return False
    return payload_data


@async
def send_mail(subject, sender, recipients, text, html):
    r = requests.post(
        MAILGUN_BASE_URL + '/messages',
        auth=('api', MAILGUN_API_KEY),
        data={'from': sender,
              'to': recipients,
              'subject': subject,
              'text': text,
              'html': html,
              'o:tracking': False})
    # mail didn't send? uncomment following line, keep an eye on terminal
    print(r.text)


def send_register_email(email):
    link = get_authorised_link('verify_email', email=email)
    send_mail('Complete your registration',
              NOTIFICATIONS,
              [email],
              render_template('emails/register.txt', email=email, link=link),
              render_template('emails/register.html', email=email, link=link))


def send_dupe_email(email):
    link = get_authorised_link('reset_password', email=email)
    send_mail('Repeat account registration attempt',
              NOTIFICATIONS,
              [email],
              render_template('emails/notify_dupe.txt', email=email,
                              link=link),
              render_template('emails/notify_dupe.html', email=email,
                              link=link))


def send_reset_link(email):
    link = get_authorised_link('reset_password', email=email)
    send_mail('Complete your password reset', NOTIFICATIONS, [email],
              render_template('emails/reset_password.txt', email=email,
                              link=link),
              render_template('emails/reset_password.html', email=email,
                              link=link))


def send_password_change_notification(email):
    send_mail('Your password has changed', NOTIFICATIONS, [email],
              render_template('emails/notify_password_change.txt',
                              email=email),
              render_template('emails/notify_password_change.html',
                              email=email))
