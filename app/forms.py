from datetime import datetime
from decimal import Decimal

from flask_wtf import FlaskForm
from wtforms import FormField, StringField, BooleanField, \
    PasswordField, SelectField
from wtforms import Form as NoCsrfForm
from wtforms.validators import Length, InputRequired, EqualTo, Email, \
    StopValidation, Optional

from .constants import REPEAT, ACTION


# VALIDATORS
def currency(form, field):
    try:
        Decimal(field.data)
    except:
        raise StopValidation('Value must be in decimal format')
    if Decimal(field.data) < 0:
        raise StopValidation('Value must be greater than zero')


def is_positive(form, field):
    try:
        int(field.data)
    except:
        raise StopValidation('Number must be positive or zero')
    if int(field.data) < 0:
        raise StopValidation('Number must be positive or zero')


def is_ddmmyyyy(form, field):
    # Only validate if a value has been provided.
    if field.data:
        try:
            datetime.strptime(field.data, '%d/%m/%Y')
        except:
            raise StopValidation('Date must be in \'DD/MM/YYYY\' format')


def is_not_historic(form, field):
    try:
        date = datetime.strptime(field.data, '%d/%m/%Y')
        date >= datetime.now().date()
    except:
        raise StopValidation('Date must occur in the future')


def make_optional(field):
    field.validators.insert(0, Optional())


class GreaterThanField(object):
    '''
    Compares the values of two fields.

    :param fieldname:
        The name of the other field to compare to.
    :param message:
        Error message to raise in case of a validation error.
    '''

    def __init__(self, fieldname):
        self.fieldname = fieldname

    def __call__(self, form, field):
        try:
            if field.data <= form[self.fieldname].data:
                raise StopValidation('Class end must be after class start')
        except:
            raise StopValidation('Class end must be after class start')


repeat = [(k, v) for k, v in REPEAT.items()]
actions = [(k, v) for k, v in ACTION.items()]


class PaymentForm(FlaskForm):
    payment_name = StringField('Name', [InputRequired(), Length(max=128)])
    payment_amount = StringField('Amount', [InputRequired(), currency,
                                            Length(max=16)])
    payment_action = SelectField('Action', [InputRequired()],
                                 choices=actions, coerce=int)
    payment_date = StringField('Date', [InputRequired(), is_ddmmyyyy,
                                        Length(max=10)])
    payment_repeat = SelectField('Repeat', [InputRequired()],
                                 choices=repeat, coerce=int, default=1)


# ACCOUNT SETTINGS
class ChangePassword(FlaskForm):
    password = PasswordField('Current password', [Length(min=8, max=256),
                             InputRequired()])
    newpassword = PasswordField('New password', [Length(min=8, max=256),
                                InputRequired()])
    confirm = PasswordField('Password again', [
        Length(min=8, max=256), InputRequired(),
        EqualTo('newpassword', message='Passwords must match')])


# AUTHENTICATION
class Signup(FlaskForm):
    name = StringField('Your name', [Length(max=128), InputRequired()])
    email = StringField('Email', [Email(message='Invalid email address'),
                                  Length(max=256), InputRequired()])
    password = PasswordField('Password', [Length(min=8, max=256),
                                          InputRequired()])
    confirm = PasswordField('Password again', [
        Length(min=8, max=256), InputRequired(),
        EqualTo('password', message='Passwords must match')])


class Login(FlaskForm):
    email = StringField('Email', [Email(message='Invalid email address'),
                                  Length(max=256), InputRequired()])
    password = PasswordField('Password', [Length(min=8, max=256),
                                          InputRequired()])
    remember_me = BooleanField('remember_me', default=False)


class ForgotPassword(FlaskForm):
    email = StringField('Enter your email address',
                        [Email(message='Invalid email address'),
                         Length(max=256), InputRequired()])


class ResetPassword(FlaskForm):
    newpassword = PasswordField('New password', [Length(min=8, max=256),
                                                 InputRequired()])
