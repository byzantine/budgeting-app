from datetime import datetime as dt
from datetime import timedelta

from sqlalchemy import Column, Boolean, DateTime, Integer, Numeric, \
    SmallInteger, String, ForeignKey
from sqlalchemy.orm import relationship

from app import db
from app.constants import *
from app.utils import today, days_in_month, days_in_period


class User(db.Model):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String(256), unique=True)
    name = Column(String(128))
    password = Column(String(256))
    last_auth = Column(DateTime, default=dt.utcnow)
    role = Column(SmallInteger, default=USER)
    status = Column(SmallInteger, default=NEW)
    # settings
    timezone = Column(String(32), default='Pacific/Auckland')
    tfa_enabled = Column(Boolean, default=False)     # 2-factor auth
    tfa_secret = Column(String(16), default=None)    # 2FA secret key
    tfa_backup = Column(String(512), default=None)   # 2FA backup codes
    # timestamps
    update_date = Column(DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    insert_date = Column(DateTime, default=dt.utcnow)
    # relationships
    payments = relationship('Payment', backref='user')

    def __init__(self, email=None, name=None, password=None,
                 role=USER):
        self.email = email
        self.name = name
        self.password = password
        self.role = role

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.status == ACTIVE

    def is_anonymous(self):
        return False

    def newly_authenticated(self):
        return self.last_auth > (dt.utcnow() - timedelta(seconds=10))

    def get_id(self):
        return self.id

    @staticmethod
    def get_user(id=None):
        return User.query.filter_by(id=id).first()

    @staticmethod
    def get_user_id(email=None):
        return User.query.filter_by(email=email).first().id

    def getStatus(self):
        return STATUS[self.status]

    def getRole(self):
        return ROLE[self.role]

    def get_payment(self, payment_id):
        payments = {p.id: p for p in self.payments}
        return payments[payment_id]

    def active_payments(self):
        return [p for p in self.payments if p.status == ACTIVE]

    def active_income(self):
        return [p for p in self.active_payments() if p.action == CREDIT]

    def active_expenses(self):
        return [p for p in self.active_payments() if p.action == DEBIT]

    def income_this_month(self):
        total = 0
        start = today().replace(day=1)
        end = start.replace(day=days_in_month(start))
        for i in self.active_income():
            total += i.amount * i.occurrences(start, end)
        return total

    def income_this_year(self):
        total = 0
        start = today().replace(month=1, day=1)
        end = start.replace(month=12, day=31)
        for i in self.active_income():
            total += i.amount * i.occurrences(start, end)
        return total

    def expenses_this_month(self):
        total = 0
        start = today().replace(day=1)
        end = start.replace(day=days_in_month(start))
        for e in self.active_expenses():
            total += e.amount * e.occurrences(start, end)
        return total

    def expenses_this_year(self):
        total = 0
        start = today().replace(month=1, day=1)
        end = start.replace(month=12, day=31)
        for e in self.active_expenses():
            total += e.amount * e.occurrences(start, end)
        return total

    def __repr__(self):
        return '<User {} {}>'.format(self.id, self.email)


class Payment(db.Model):
    __tablename__ = 'payments'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    status = Column(SmallInteger, default=ACTIVE)
    name = Column(String(128))
    description = Column(String(1024))
    amount = Column(Numeric(scale=2), default=0)
    action = Column(SmallInteger)
    date = Column(DateTime)
    repeat = Column(SmallInteger)
    # timestamps
    update_date = Column(DateTime, default=dt.utcnow, onupdate=dt.utcnow)
    insert_date = Column(DateTime, default=dt.utcnow)

    def __init__(self, user_id, name, amount, action, date, repeat='weekly'):
        self.user_id = user_id
        self.name = name
        self.amount = amount
        self.action = action
        self.date = date
        self.repeat = repeat

    def cancel(self):
        self.status = INACTIVE

    def activate(self):
        self.status = ACTIVE

    def occurrences(self, start, end):
        '''
        Return number of payment occurrences in given timeframe.
        '''
        end = days_in_period(start, end)
        if self.repeat == DAILY:
            # Days in given month.
            return end
        elif self.repeat == WEEKLY:
            to_end = len(range(self.date.day, end + 1, 7))
            to_start = len(range(self.date.day, 0, -7))
            return (to_end + to_start) - 1
        elif self.repeat == TWO_WEEKLY:
            to_end = len(range(self.date.day, end + 1, 14))
            to_start = len(range(self.date.day, 0, -14))
            return (to_end + to_start) - 1
        elif self.repeat == FOUR_WEEKLY:
            return 1
            # TODO: need to do this properly.
            raise Exception('TODO')
        elif self.repeat == MONTHLY_SAME:
            return 1
        elif self.repeat == ANNUAL:
            if self.date.month == given_date.month:
                return 1

    def __repr__(self):
        return '<{} {}>'.format(ACTION[self.action], self.id)
