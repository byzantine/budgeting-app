# This Python file uses the following encoding: utf-8
from datetime import date
import calendar

from app import app


def today():
    '''
    Return today's date.
    '''
    return date.today()


def days_in_month(given_date):
    '''
    Return last day of month of given date.
    '''
    _, last_day = calendar.monthrange(given_date.year, given_date.month)
    return last_day


def days_in_period(start, end):
    '''
    Given two date or datetime objects it will return the difference in days.
    If start is 01/05/17 & end is 31/05/17, it will return 31.
    '''
    return (end - start).days


def current_month_start_end():
    '''
    Return tuple of date objects representing (start, end) of current month.
    '''
    start = date.today().replace(day=1)
    _, last_day = calendar.monthrange(start.year, start.month)
    end = start.replace(day=last_day)
    return start, end


def in_current_month(dt):
    '''
    Returns True if given datetime object in current month, otherwise False.
    '''
    start, end = current_month_start_end()
    if dt >= start and dt <= end:
        return True
    return False


@app.template_filter('format_nzd')
def format_nzd(price):
    return '{:,.2f}'.format(price)
